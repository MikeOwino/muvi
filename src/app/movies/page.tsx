import Nav from '@/components/nav'
import Footer from '@/components/footer'
import Image from 'next/image'

export default function Movies() {
  return (
    <>
      <Nav />
      <br />
      <div className="grid grid-cols-4 gap-4 px-2.5">
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500 grid place-content-center row-span-3">01</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500 grid place-content-center row-span-3">02</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500 grid place-content-center row-span-3">03</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">04</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">05</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">06</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">07</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">08</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">09</div>
        <div className="p-4 rounded-lg shadow-lg bg-fuchsia-500">10</div>
      </div>
      <br />
      <Footer />
    </>
  )
}
