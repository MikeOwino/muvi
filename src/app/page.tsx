// import Image from 'next/image'
// import Head from 'next/head'
import Nav from '../components/nav'
import Footer from '@/components/footer'
import Image from 'next/image'

export default function Home() {
  return (
    <>
      <Nav />
      <div className="carousel w-full">
        <div id="slide1" className="carousel-item relative w-full brightness-50">
          <Image src="https://www.videogameschronicle.com/files/2023/02/mario-movie.jpg" alt='foto' width={1280} height={720} className="w-full" />
          <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
            <a href="#slide4" className="btn btn-circle">❮</a>
            <a href="#slide2" className="btn btn-circle">❯</a>
          </div>
        </div>
        <div id="slide2" className="carousel-item relative w-full">
          <Image src="https://daisyui.com/images/stock/photo-1609621838510-5ad474b7d25d.jpg" alt='foto' width={1280} height={720} className="w-full" />
          <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
            <a href="#slide1" className="btn btn-circle">❮</a>
            <a href="#slide3" className="btn btn-circle">❯</a>
          </div>
        </div>
        <div id="slide3" className="carousel-item relative w-full">
          <Image src="https://daisyui.com/images/stock/photo-1414694762283-acccc27bca85.jpg" alt='foto' width={1280} height={720} className="w-full" />
          <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
            <a href="#slide2" className="btn btn-circle">❮</a>
            <a href="#slide4" className="btn btn-circle">❯</a>
          </div>
        </div>
        <div id="slide4" className="carousel-item relative w-full">
          <Image src="https://daisyui.com/images/stock/photo-1665553365602-b2fb8e5d1707.jpg" alt='foto' width={1280} height={720} className="w-full" />
          <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
            <a href="#slide3" className="btn btn-circle">❮</a>
            <a href="#slide1" className="btn btn-circle">❯</a>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
