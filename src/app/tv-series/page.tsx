import Nav from '@/components/nav'
import Footer from '@/components/footer'
import Image from 'next/image'

export default function Tv_series() {
  return (
    <>
      <Nav />
      <br />
      <div className="flex items-center justify-around gap-6 sm:gap-4 font-mono font-bold shrink-0 p-8">
        <div className="flex flex-col items-center shrink-0">
          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-50 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-50</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-125 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-125</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-around gap-6 sm:gap-4 font-mono font-bold shrink-0 p-8">
        <div className="flex flex-col items-center shrink-0">
          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-50 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-50</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-125 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-125</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-around gap-6 sm:gap-4 font-mono font-bold shrink-0 p-8">
        <div className="flex flex-col items-center shrink-0">
          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-50 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-50</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-125 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-125</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
        <div className="flex flex-col items-center shrink-0">

          <div className="relative">
            <div className="absolute inset-6 backdrop-saturate-200 h-20 w-20 bg-white/30"></div>
            <Image className="w-32 h-32 object-cover rounded-lg shadow-xl" src="https://images.unsplash.com/photo-1684364971476-490f5b033aed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt='alslask' width={800} height={800} />
            <p className="font-medium text-sm text-slate-800 font-mono text-center mb-3 dark:text-slate-400">backdrop-saturate-200</p>
            <div className="absolute inset-0 ring-1 ring-inset ring-black/10 rounded-lg"></div>
          </div>
        </div>
      </div>
      <br />
      <Footer />
    </>
  )
}
