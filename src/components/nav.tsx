import Image from 'next/image'
import Link from 'next/link'
import imdb from './db.svg'
import pro from './pro.svg'


export default function Nav() {
    return (
        <div data-theme="black" className="navbar bg-base-100">
            <div className="navbar-start">
                <div className="dropdown">
                    <label tabIndex={0} className="btn btn-ghost lg:hidden">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
                    </label>
                    <ul tabIndex={0} className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52">
                        <li><Link href='/movies'>Movies</Link></li>
                        <li><Link href='/tv-series'>TV Series</Link></li>
                        <li><Link href='/IMDB-top-rated'>Top IMDB</Link></li>
                        <li tabIndex={0}>
                            <Link href='/top-rated-movies' className="justify-between">
                                Top Rated
                                {/* <svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" /></svg> */}
                            </Link>
                            {/* <ul className="p-2">
                                <li><a>Submenu 1</a></li>
                                <li><a>Submenu 2</a></li>
                            </ul> */}
                        </li>
                        <li><Link href='/request-title'>Request Title</Link></li>
                    </ul>
                </div>
                <Link className="btn btn-ghost normal-case text-xl" href='/'><Image src={imdb} alt='imdb logo' /></Link>
            </div>
            <div className="navbar-center hidden lg:flex">
                <ul className="menu menu-horizontal px-1">
                    <li><Link href='/movies'>Movies</Link></li>
                    <li><Link href='/tv-series'>TV Series</Link></li>
                    <li><Link href='/IMDB-top-rated'>Top IMDB</Link></li>
                    <li tabIndex={0}>
                        <Link href='/top-rated-movies'>
                            Top Rated
                            {/* <svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" /></svg> */}
                        </Link>
                        {/* <ul className="p-2">
                            <li><a>Submenu 1</a></li>
                            <li><a>Submenu 2</a></li>
                        </ul> */}
                    </li>
                    <li><Link href='/request-title'>Request Title</Link></li>

                </ul>
            </div>
            <div className="navbar-end">
                <Link href='/auth/login' className="btn"><Image src={pro} alt='imdb logo' /></Link>
            </div>
        </div>
    )
}
